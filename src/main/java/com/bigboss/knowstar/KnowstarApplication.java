package com.bigboss.knowstar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnowstarApplication {

    public static void main(String[] args) {
        SpringApplication.run(KnowstarApplication.class, args);
    }

}
