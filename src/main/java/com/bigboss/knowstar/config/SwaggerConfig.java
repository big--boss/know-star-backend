package com.bigboss.knowstar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(true)
                /**
                 * enable(false)
                 * 表示禁用swagger
                 */
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.bigboss.knowstar.controller"))
                /**
                 * RequestHandlerSelectors 规定要扫描接口的方式
                 * basePackage：指定要扫描的包
                 * any：扫描所有的
                 * none：不扫描
                 * withClassAnnotation：扫描类上的注解
                 * withMethodAnnotation：扫描方法上的注解
                 */
                .paths(PathSelectors.any())
                /**
                 * PathSelectors过滤路径
                 * ant：指定路径
                 * any：扫描所有
                 */
                .build();
    }

    public ApiInfo apiInfo(){

        Contact contact = new Contact("bigboss", "www.baidu.com", "111111@qq.com");

        return new ApiInfoBuilder()
                .title("知识共享平台API文档")
                .description("用restful风格写接口")
                .contact(contact)
                .version("1.0")
                .build();
    }
}
