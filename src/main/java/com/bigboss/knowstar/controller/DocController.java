package com.bigboss.knowstar.controller;

import com.bigboss.knowstar.entity.Doc;
import com.bigboss.knowstar.entity.Tag;
import com.bigboss.knowstar.entity.model.APIResult;
import com.bigboss.knowstar.exception.DocException;
import com.bigboss.knowstar.exception.TagException;
import com.bigboss.knowstar.service.impl.DocServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bigboss.knowstar.utils.ValidateData.validateData;

@RestController
@RequestMapping("/doc")
public class DocController {

    @Autowired
    private DocServiceImpl docService;

    @PostMapping("/{Fid}")
    public APIResult docCreate(@PathVariable Integer Fid, @RequestBody @Validated Doc doc, BindingResult result) {
        try {
            ArrayList<String> data = validateData(result);
            if (data != null) {
                return APIResult.error(data);
            }
            docService.createDoc(Fid, doc);
            return APIResult.success("文档创建成功");
        } catch (DocException e) {
            return APIResult.error("文档创建失败:" + e.getMessage());
        }

    }

    @PutMapping("/{Did}")
    public APIResult docUpdate(@PathVariable Integer Did, @RequestBody Map<String, Object> fields) {
        try {
            docService.updateDoc(Did, fields);
            return APIResult.success("文档更新成功");
        } catch (DocException e) {
            return APIResult.error("文档更新失败:" + e.getMessage());
        }
    }

    @DeleteMapping("/{Did}")
    public APIResult docDelete(@PathVariable Integer Did) {
        try {
            docService.deleteDoc(Did);
            return APIResult.success("文档删除成功");
        } catch (DocException e) {
            return APIResult.error("文档删除失败:" + e.getMessage());
        }
    }

    @GetMapping("/{Did}")
    public APIResult docQueryByFid(@PathVariable Integer Did) {
        try {
            Doc doc = docService.getDoc(Did);
            return APIResult.success(doc);
        } catch (DocException e) {
            return APIResult.error("文档查询失败:" + e.getMessage());
        }
    }

    @PostMapping("/tag/{Did}")
    public APIResult docTagCreate(@PathVariable Integer Did, @RequestBody @Validated Tag tag, BindingResult result) {
        try {
            ArrayList<String> data = validateData(result);
            if (data != null) {
                return APIResult.error(data);
            }
            docService.createDocTag(Did, tag);
            return APIResult.success("文档标签创建成功");
        } catch (TagException e) {
            return APIResult.error("文档标签创建失败:" + e.getMessage());
        }
    }

    @GetMapping("/tag/{Did}")
    public APIResult docTagQueryById(@PathVariable Integer Did) {
        try {
            List<Tag> docTags = docService.getDocTags(Did);
            return APIResult.success(docTags);
        } catch (TagException e) {
            return APIResult.error("文档标签查询失败:" + e.getMessage());
        }
    }
}