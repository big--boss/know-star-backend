package com.bigboss.knowstar.controller;

import com.bigboss.knowstar.entity.Doc;
import com.bigboss.knowstar.entity.Folder;
import com.bigboss.knowstar.entity.Tag;
import com.bigboss.knowstar.entity.model.APIResult;
import com.bigboss.knowstar.entity.model.PageData;
import com.bigboss.knowstar.exception.FolderException;
import com.bigboss.knowstar.exception.TagException;
import com.bigboss.knowstar.service.impl.FolderServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bigboss.knowstar.utils.ValidateData.validateData;

@RestController
@RequestMapping("/folder")
public class FolderController {
    
    @Autowired
    private FolderServiceImpl folderService;

    @PostMapping("")
    public APIResult folderCreate(@RequestBody @Validated Folder folder, BindingResult result) {
        try {
            ArrayList<String> data = validateData(result);
            if (data != null) {
                return APIResult.error(data);
            }
            folderService.createFolder(folder);
            return APIResult.success("专栏创建成功");
        } catch (FolderException e) {
            return APIResult.error("专栏创建失败:" + e.getMessage());
        }

    }

    @PutMapping("/{Fid}")
    public APIResult folderUpdate(@PathVariable Integer Fid, @RequestBody Map<String, Object> fields) {
        try {
            folderService.updateFolder(Fid, fields);
            return APIResult.success("专栏更新成功");
        } catch (FolderException e) {
            return APIResult.error("专栏更新失败:" + e.getMessage());
        }
    }

    @DeleteMapping("/{Fid}")
    public APIResult folderDelete(@PathVariable Integer Fid) {
        try {
            folderService.deleteFolder(Fid);
            return APIResult.success("专栏删除成功");
        } catch (FolderException e) {
            return APIResult.error("专栏删除失败:" + e.getMessage());
        }
    }

    @GetMapping("/{Fid}")
    public APIResult folderQueryByFid(@PathVariable Integer Fid) {
        try {
            Folder folder = folderService.getFolder(Fid);
            return APIResult.success(folder);
        } catch (FolderException e) {
            return APIResult.error("专栏查询失败:" + e.getMessage());
        }
    }

    @GetMapping("/docs/{Fid}")
    public APIResult queryAllDocInFolder(@PathVariable Integer Fid, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer limit) {
        PageHelper.startPage(page, limit);
        List<Doc> docs = folderService.getAllDocInFolder(Fid);
        return APIResult.success(PageData.fromPage((Page<Doc>) docs));
    }

    @PostMapping("/tag/{Fid}")
    public APIResult folderTagCreate(@PathVariable Integer Fid, @RequestBody @Validated Tag tag, BindingResult result) {
        try {
            ArrayList<String> data = validateData(result);
            if (data != null) {
                return APIResult.error(data);
            }
            folderService.createFolderTag(Fid, tag);
            return APIResult.success("专栏标签创建成功");
        } catch (TagException e) {
            return APIResult.error("专栏标签创建失败:" + e.getMessage());
        }
    }

    @GetMapping("/tag/{Fid}")
    public APIResult folderTagQueryById(@PathVariable Integer Fid) {
        try {
            List<Tag> folderTags = folderService.getFolderTags(Fid);
            return APIResult.success(folderTags);
        } catch (TagException e) {
            return APIResult.error("专栏标签查询失败:" + e.getMessage());
        }
    }
}
