package com.bigboss.knowstar.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/index")
    public String hello() {
        return "这是首页！！！";
    }

    @RequestMapping("/login_page")
    public String loginPage() {
        return "未登录，请登录！！！";
    }
}
