package com.bigboss.knowstar.controller;

import com.bigboss.knowstar.utils.VerificationCode;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

@RestController
public class LoginController {

    @GetMapping("/verifyCode")
    public void verifyCode(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        VerificationCode code = new VerificationCode();
        BufferedImage image = code.getImage();
        HttpSession session = req.getSession(true);
        session.setAttribute("verify_code", code.getText());
        VerificationCode.output(image, resp.getOutputStream());
    }
}
