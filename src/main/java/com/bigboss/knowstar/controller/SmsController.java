package com.bigboss.knowstar.controller;

import com.bigboss.knowstar.entity.model.APIResult;
import com.bigboss.knowstar.service.impl.SmsServiceImpl;
import com.bigboss.knowstar.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/sms")
public class SmsController {

    @Autowired
    private SmsServiceImpl smsService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @GetMapping("/{Uphone}/{code}")
    public APIResult sendSms(HttpServletRequest req, @PathVariable String Uphone, @PathVariable String code) {
        String verify_code = (String) req.getSession().getAttribute("verify_code");
        if (StringUtils.isEmpty(verify_code) || StringUtils.isEmpty(code) || !verify_code.toLowerCase().equals(code.toLowerCase())) {
            //验证码不正确
            return APIResult.error("验证码错误");
        }
        if (!userService.isPhoneExist(Uphone)) {
            return APIResult.error("电话号码不存在");
        }
        String phoneCode = redisTemplate.opsForValue().get(Uphone);
        if (!StringUtils.isEmpty(phoneCode)) {
            return APIResult.error("短信验证码已存在");
        }
        phoneCode  = UUID.randomUUID().toString().substring(0, 4);
        HashMap<String, Object> params = new HashMap<>();
        params.put("code", phoneCode);
        boolean isSendSuccess = smsService.send(Uphone, "SMS_200703217", params);
        if (isSendSuccess) {
            redisTemplate.opsForValue().set(Uphone, phoneCode, 5, TimeUnit.MINUTES);
            return APIResult.success("短信验证码发送成功");
        } else {
            return APIResult.error("短信验证码发送失败");
        }
    }

    @GetMapping("/verify/{Uphone}/{code}")
    public APIResult verifySms(@PathVariable String Uphone, @PathVariable String code) {
        if (StringUtils.isEmpty(Uphone)) {
            return APIResult.error("手机号码为空");
        }
        if (StringUtils.isEmpty(code)) {
            return APIResult.error("短信验证码为空");
        }
        String verifyCode = redisTemplate.opsForValue().get(Uphone);
        if (StringUtils.isEmpty(verifyCode)) {
            return APIResult.error("短信验证码不存在");
        } else {
            if (verifyCode.toLowerCase().equals(code.toLowerCase())) {
                return APIResult.success("短信验证码验证成功");
            } else {
                return APIResult.error("短信验证码验证失败");
            }
        }
    }
}
