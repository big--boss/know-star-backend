package com.bigboss.knowstar.controller;

import com.bigboss.knowstar.entity.model.APIResult;
import com.bigboss.knowstar.exception.FileUploadException;
import com.bigboss.knowstar.service.impl.LocalUploadServiceImpl;
import com.bigboss.knowstar.service.impl.OSSUploadServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class UploadController {

    @Autowired
    private LocalUploadServiceImpl localUploadService;

    @Autowired
    private OSSUploadServiceImpl ossUploadService;

    @PostMapping("/upload/{type}")
    public APIResult uploadFileToLocal(MultipartFile file, HttpServletRequest req, @PathVariable String type) {
        try {
            String filePath = localUploadService.uploadFile(file, req, type);
            return APIResult.success("文件上传成功:" + filePath);
        } catch (FileUploadException e) {
            return APIResult.error("文件上传失败:" + e.getMessage());
        }
    }

    @PostMapping("/uploads/{type}")
    public APIResult uploadFilesToLocal(MultipartFile[] files, HttpServletRequest req, @PathVariable String type) {
        try {
            List<String> filePaths = localUploadService.multiUploadFiles(files, req, type);
            return APIResult.success(filePaths);
        } catch (FileUploadException e) {
            return APIResult.error(e.getMessage());
        }
    }

    @PostMapping("/oss/upload/{type}")
    public APIResult uploadFileToOSS(MultipartFile file, HttpServletRequest req, @PathVariable String type) {
        try {
            String filePath = ossUploadService.uploadFile(file, req, type);
            return APIResult.success(filePath);
        } catch (FileUploadException e) {
            return APIResult.error(e.getMessage());
        }
    }

    @PostMapping("/oss/uploads/{type}")
    public APIResult uploadFilesToOSS(MultipartFile[] files, HttpServletRequest req, @PathVariable String type) {
        try {
            List<String> filePaths = ossUploadService.multiUploadFiles(files, req, type);
            return APIResult.success(filePaths);
        } catch (FileUploadException e) {
            return APIResult.error(e.getMessage());
        }
    }
}
