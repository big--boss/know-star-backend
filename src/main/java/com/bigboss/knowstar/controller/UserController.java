package com.bigboss.knowstar.controller;

import com.bigboss.knowstar.entity.Tag;
import com.bigboss.knowstar.entity.User;
import com.bigboss.knowstar.entity.model.APIResult;
import com.bigboss.knowstar.exception.TagException;
import com.bigboss.knowstar.exception.UserException;
import com.bigboss.knowstar.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bigboss.knowstar.utils.ValidateData.validateData;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @PostMapping("/register")
    public APIResult userRegister(HttpServletRequest req, @RequestParam String code, @RequestBody @Validated User user, BindingResult result) {
        try {
            String verify_code = (String) req.getSession().getAttribute("verify_code");
            if (code == null || verify_code == null || "".equals(code) || !verify_code.toLowerCase().equals(code.toLowerCase())) {
                //验证码不正确
                return APIResult.error("验证码错误");
            }
            ArrayList<String> data = validateData(result);
            if (data != null) {
                return APIResult.error(data);
            }
            userService.createUser(user);
            return APIResult.success("用户注册成功");
        } catch (UserException e) {
            return APIResult.error("用户注册失败:" + e.getMessage());
        }

    }

    @PutMapping("/{Uid}")
    public APIResult userUpdate(@PathVariable Integer Uid, @RequestBody Map<String, Object> fields) {
        try {
            userService.updateUser(Uid, fields);
            return APIResult.success("用户更新成功");
        } catch (UserException e) {
            return APIResult.error("用户更新失败:" + e.getMessage());
        }
    }

    @DeleteMapping("/{Uid}")
    public APIResult userDelete(@PathVariable Integer Uid) {
        try {
            userService.deleteUser(Uid);
            return APIResult.success("用户删除成功");
        } catch (UserException e) {
            return APIResult.error("用户删除失败:" + e.getMessage());
        }
    }

    @GetMapping("/{Uid}")
    public APIResult userQueryByUid(@PathVariable Integer Uid) {
        try {
            User user = userService.getUser(Uid);
            return APIResult.success(user);
        } catch (UserException e) {
            return APIResult.error("用户查询失败:" + e.getMessage());
        }
    }

    @PostMapping("/tag/{Uid}")
    public APIResult userTagCreate(@PathVariable Integer Uid, @RequestBody @Validated Tag tag, BindingResult result) {
        try {
            ArrayList<String> data = validateData(result);
            if (data != null) {
                return APIResult.error(data);
            }
            userService.createUserTag(Uid, tag);
            return APIResult.success("用户标签创建成功");
        } catch (TagException e) {
            return APIResult.error("用户标签创建失败:" + e.getMessage());
        }
    }

    @GetMapping("/tag/{Uid}")
    public APIResult userTagQueryById(@PathVariable Integer Uid) {
        try {
            List<Tag> userTags = userService.getUserTags(Uid);
            return APIResult.success(userTags);
        } catch (TagException e) {
            return APIResult.error("用户标签查询失败:" + e.getMessage());
        }
    }

    @GetMapping("/password/reset/{Uphone}/{newPassword}")
    public APIResult resetPassword(@PathVariable String Uphone, @PathVariable String newPassword) {
        try {
            userService.resetPassword(Uphone, newPassword);
            return APIResult.success("用户重置密码成功");
        } catch (UserException e) {
            return APIResult.error("用户重置密码失败:" + e.getMessage());
        }
    }
}
