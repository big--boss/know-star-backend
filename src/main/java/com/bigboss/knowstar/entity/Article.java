package com.bigboss.knowstar.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class Article {
    private Integer Aid;
    @NotNull(message = "{article.Atitle.notnull}")
    private String Atitle;
    @NotNull(message = "{article.Aimage.notnull}")
    private String Aimage;
    @NotNull(message = "{aritcle.Asource.notnull}")
    private String Asource;
    @NotNull(message = "{aritcle.Acategory.notnull}")
    private String Acategory;
    private Integer Aviews;
    private Integer Alikes;
    private Integer Acollections;
    private Date AcreateTime = new Date();
    private Boolean Avisible;
}
