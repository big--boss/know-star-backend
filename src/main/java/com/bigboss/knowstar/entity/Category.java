package com.bigboss.knowstar.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class Category {
    private Integer Cid;
    @NotNull(message = "{category.Cname.notnull}")
    private String Cname;
    private Date CcreateTime;
    private boolean Cvisible;
}
