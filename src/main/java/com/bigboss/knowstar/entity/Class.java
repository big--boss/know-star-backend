package com.bigboss.knowstar.entity;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
public class Class {
    private Integer ClassId;
    @NotNull(message = "{_class.ClassName.notnull}")
    private String ClassName;
    private Integer ClassAdminId;
    private String ClassAvatar;
    @NotNull(message = "{_class.ClassEmail.notnull}")
    @Email(message = "{_class.ClassEmail.parttern}")
    private String ClassEmail;
    @NotNull(message = "{_class.ClassPhone.notnull}")
    @Size(min = 11, max = 11, message = "{_class.ClassPhone.size}")
    private String ClassPhone;
    private String ClassDescription;
    private Integer ClassStuNum;
    private Double ClassSubmitRate;
    private Date ClassCreateTime;
    private Boolean ClassVisible;
}
