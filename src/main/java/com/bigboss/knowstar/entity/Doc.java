package com.bigboss.knowstar.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class Doc {
    private Integer Did;
    @NotNull(message = "{doc.Dname.notnull}")
    private String Dname;
    @NotNull(message = "{doc.Dtype.notnull}")
    private String Dtype;
    @NotNull(message = "{doc.Dpath.notnull}")
    private String Dpath;
    private Date DcreateTime = new Date();
    private Integer Dlikes;
    private Integer Ddownloads;
    private Integer Dcollections;
    private String Ddescription;
    private Boolean Dvisible;
    private Long Dsize;

    // 外键
    private Integer DFid;
}
