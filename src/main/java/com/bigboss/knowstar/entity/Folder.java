package com.bigboss.knowstar.entity;

import io.swagger.models.auth.In;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class Folder {
    private Integer Fid;
    @NotNull(message = "{folder.Fname.notnull}")
    private String Fname;
    @NotNull(message = "{folder.Fdescription.notnull}")
    private String Fdescription;
    private String Fimage;
    private Date FcreateTime = new Date();
    private Integer Flikes;
    private Integer Fdownloads;
    private Integer Fcollections;
    private Boolean Fvisible;

    // 外键
    private Integer FUid;
}
