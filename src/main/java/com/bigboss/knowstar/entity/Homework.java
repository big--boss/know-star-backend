package com.bigboss.knowstar.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class Homework {
    private Integer Hid;
    @NotNull(message = "{homework.Htitle.notnull}")
    private String Htitle;
    @NotNull(message = "{homework.Hdescription.notnull}")
    private String Hdescription;
    private Date HcreateTime;
    private Date HstartTime;
    private Date HendTime;
    private Integer Hsubmits;
    private Boolean Hvisible;

    // 外键
    private Integer HClassId;
}
