package com.bigboss.knowstar.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Notice {
    private Integer Nid;
    private String Ntitle;
    private String Ncontent;
    private Date NcreateTime;
    private Boolean Nvisible;

    // 外键
    private Integer NClassId;
}
