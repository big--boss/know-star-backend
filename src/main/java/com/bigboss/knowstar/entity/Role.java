package com.bigboss.knowstar.entity;

import lombok.Data;

@Data
public class Role {
    private Integer Rid;
    private String Rname;
    private String Rdescription;
}
