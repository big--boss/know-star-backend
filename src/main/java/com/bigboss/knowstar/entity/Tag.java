package com.bigboss.knowstar.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Tag {
    private Integer Tid;
    private String Tname;
    private Date TcreateTime;
    private boolean Tvisible;
}
