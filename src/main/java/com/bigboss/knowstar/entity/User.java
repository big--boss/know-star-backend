package com.bigboss.knowstar.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserDetails {
    private Integer Uid;
    @Size(min = 10, max = 10, message = "{user.UStuId.size}")
    private String UStuId;
    @NotNull(message = "{user.Uname.notnull}")
    private String Uname;
    @NotNull(message = "{user.Upasswword.notnull}")
    private String Upassword;
    private String Ugender;
    @NotNull(message = "{user.UrealName.notnull}")
    private String UrealName;
    private String Uphone;
    private String Udescription;
    private String Uannouncement;
    private String Uavatar;
    private Boolean Uvisible = true;
    private Date UcreateTime = new Date();
    private Date UlastLogin;
    private Integer Uvisits;
    private Integer Uviews;
    private Integer Udownloads;
    private Integer Ulikes;
    private Integer Ufans;
    private Integer Ufollows;
    // 外键
    private Integer UClassId;


    @JsonIgnore
    private List<Role> roles = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getRname()));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return Upassword;
    }

    @Override
    public String getUsername() {
        return Uname;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return Uvisible;
    }
}
