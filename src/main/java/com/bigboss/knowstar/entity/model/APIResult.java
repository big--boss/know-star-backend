package com.bigboss.knowstar.entity.model;

import com.bigboss.knowstar.utils.Constant;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 如果请求成功：status则为success
 * 如果请求失败：status则为error
 * data为请求返回的message
 */

@Data
@AllArgsConstructor
public class APIResult {

    private String status;
    private Object data;

    public static APIResult success() {
        return new APIResult(Constant.DEFAULT_SUCCESS_MESSAGE, null);
    }

    public static APIResult success(Object data) {
        return new APIResult(Constant.DEFAULT_SUCCESS_MESSAGE, data);
    }

    public static APIResult error() {
        return new APIResult(Constant.DEFAULT_ERROR_MESSAGE, null);
    }

    public static APIResult error(Object data) {
        return new APIResult(Constant.DEFAULT_ERROR_MESSAGE, data);
    }
}
