package com.bigboss.knowstar.entity.model;

import com.bigboss.knowstar.entity.Class;
import com.bigboss.knowstar.entity.Notice;
import com.bigboss.knowstar.entity.User;
import lombok.Data;

@Data
public class ClassItemData {
    private Class _class;
    private User classAdmin;
    private Notice classLatestNotice;
}
