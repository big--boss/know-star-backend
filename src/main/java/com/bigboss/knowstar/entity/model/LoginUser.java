package com.bigboss.knowstar.entity.model;

import org.springframework.security.core.context.SecurityContextHolder;

public class LoginUser {
    public static Object getLoginUser() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
