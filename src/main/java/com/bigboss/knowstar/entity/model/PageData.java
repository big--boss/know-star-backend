package com.bigboss.knowstar.entity.model;

import com.github.pagehelper.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageData {

    private Long total;

    private List<?> page;

    public static PageData fromPage(Page<?> page) {
        return new PageData(page.getTotal(), page);
    }

    public static PageData fromList(List<?> list, int page, int limit) {
        int startIndex = (page - 1) * limit, endIndex = startIndex + (limit == 0 ? list.size() : limit);
        if (startIndex > list.size()) {
            startIndex = list.size();
        }
        if (endIndex > list.size()) {
            endIndex = list.size();
        }
        return new PageData((long) list.size(), list.subList(startIndex, endIndex));
    }
}

