package com.bigboss.knowstar.exception;

public class ArticleException extends BaseException {
    public ArticleException(String message) {
        super("Article Exception: ", message);
    }
}
