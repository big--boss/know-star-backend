package com.bigboss.knowstar.exception;

import com.bigboss.knowstar.utils.Constant;
import lombok.Data;

@Data
public class BaseException extends RuntimeException {
    private String error = Constant.DEFAULT_ERROR_MESSAGE;

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(String error, String message) {
        super(message);
        this.error = error;
    }

    public BaseException(String error, String message, Throwable cause) {
        super(message, cause);
        this.error = error;
    }
}
