package com.bigboss.knowstar.exception;

public class ClassException extends BaseException {
    public ClassException(String message) {
        super("Class Exception: ", message);
    }

}
