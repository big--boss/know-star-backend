package com.bigboss.knowstar.exception;

public class DocException extends BaseException {
    public DocException(String message) {
        super("Doc Exception: ", message);
    }
}
