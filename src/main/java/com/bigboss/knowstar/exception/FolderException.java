package com.bigboss.knowstar.exception;

public class FolderException extends BaseException{
    public FolderException(String message) {
        super("Folder Exception: ", message);
    }
}
