package com.bigboss.knowstar.exception;

import com.bigboss.knowstar.entity.model.APIResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 处理 BaseException（以及其子类）并返回对应的错误响应
     */
    @ExceptionHandler(BaseException.class)
    public APIResult handleBaseException(BaseException e) {
//        logException(e);
        return new APIResult(e.getError(), e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public APIResult handleException(Exception e) {
//        logException(e);
        return APIResult.error("未知错误：" + e.getClass().getName() + ": " + e.getMessage());
    }
}
