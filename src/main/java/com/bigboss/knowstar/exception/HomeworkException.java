package com.bigboss.knowstar.exception;

public class HomeworkException extends BaseException {
    public HomeworkException(String message) {
        super("Homework Exception: ", message);
    }

}
