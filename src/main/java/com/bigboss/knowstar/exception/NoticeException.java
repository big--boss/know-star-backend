package com.bigboss.knowstar.exception;

public class NoticeException extends BaseException {
    public NoticeException(String message) {
        super("Notice Exception: ", message);
    }

}
