package com.bigboss.knowstar.exception;

public class TagException extends BaseException {
    public TagException(String message) {
        super("Tag Exception: ", message);
    }
}
