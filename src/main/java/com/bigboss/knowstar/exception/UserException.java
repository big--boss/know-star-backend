package com.bigboss.knowstar.exception;

public class UserException extends BaseException {
    public UserException(String message) {
        super("User Exception: ", message);
    }
}
