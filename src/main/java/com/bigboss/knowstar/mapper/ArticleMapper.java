package com.bigboss.knowstar.mapper;

import com.bigboss.knowstar.entity.Article;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ArticleMapper {
    int createArticle(Article article);
    int updateArticle(Integer Aid, Map<String, Object> fields);
    int deleteArticle(Integer Aid);
    Article getArticle(Integer Aid);
    List<Article> getAllArticle();
}
