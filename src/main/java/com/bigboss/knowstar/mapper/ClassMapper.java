package com.bigboss.knowstar.mapper;

import com.bigboss.knowstar.entity.*;
import com.bigboss.knowstar.entity.Class;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ClassMapper {
    int createClass(Class class_);
    int updateClass(Integer ClassId, Map<String, Object> fields);
    int deleteClass(Integer ClassId);
    Class getClass(Integer ClassId);
    List<Class> getAllClasses();
    List<Class> getAllClassesByGrade(Integer grade);
    List<User> getClassMembers(Integer ClassId);
    List<Homework> getClassHomework(Integer ClassId);
    List<Notice> getClassNotices(Integer ClassId);
    int addClassMember(Integer ClassId, String UStuId);
    int isMemberExist(Integer ClassId, String UStuId);
}
