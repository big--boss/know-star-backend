package com.bigboss.knowstar.mapper;

import com.bigboss.knowstar.entity.Doc;
import com.bigboss.knowstar.entity.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface DocMapper {
    int createDoc(Doc doc);
    int updateDoc(Integer Did, Map<String, Object> fields);
    int deleteDoc(Integer Did);
    Doc getDoc(Integer Did);
    int createDocTag(Integer Did, Integer Tid);
    List<Tag> getDocTags(Integer Did);
}
