package com.bigboss.knowstar.mapper;

import com.bigboss.knowstar.entity.Doc;
import com.bigboss.knowstar.entity.Folder;
import com.bigboss.knowstar.entity.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface FolderMapper {
    int createFolder(Folder folder);
    int updateFolder(Integer Fid, Map<String, Object> fields);
    int deleteFolder(Integer Fid);
    Folder getFolder(Integer Fid);
    List<Folder> getAllFolder();
    List<Doc> getAllDocInFolder(Integer Fid);
    int createFolderTag(Integer Fid, Integer Tid);
    List<Tag> getFolderTags(Integer Fid);
}
