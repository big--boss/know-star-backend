package com.bigboss.knowstar.mapper;

import com.bigboss.knowstar.entity.Class;
import com.bigboss.knowstar.entity.Homework;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface HomeworkMapper {
    int createHomework(Homework homework);
    int updateHomework(Integer Hid, Map<String, Object> fields);
    int deleteHomework(Integer Hid);
    Homework getHomework(Integer Hid);
}
