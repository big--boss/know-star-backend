package com.bigboss.knowstar.mapper;

import com.bigboss.knowstar.entity.Notice;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
@Repository
public interface NoticeMapper {
    int createNotice(Notice notice);
    int updateNotice(Integer Nid, Map<String, Object> fields);
    int deleteNotice(Integer Nid);
    Notice getNotice(Integer Nid);
    Notice getLatestNotice();
}
