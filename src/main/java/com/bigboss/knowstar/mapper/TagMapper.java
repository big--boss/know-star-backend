package com.bigboss.knowstar.mapper;

import com.bigboss.knowstar.entity.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TagMapper {
    int createTag(Tag tag);
    int updateTag(Integer Tid, Map<String, Object> fields);
    int deleteTag(Integer Tid);
    Tag getTag(Integer Tid);
    Tag getTagByTname(String Tname);
    List<Tag> getTagsByUid(Integer Uid);// 查询某个用户的所有标签
    List<Tag> getTagsByFid(Integer Fid);// 查询某个专栏的所有标签
    List<Tag> getTagsByAid(Integer Aid);// 查询某篇文章的所有标签
    int isTnameExist(String Tname);// 判断某个标签名是否存在。不存在返回0
}
