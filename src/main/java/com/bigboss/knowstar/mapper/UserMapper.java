package com.bigboss.knowstar.mapper;

import com.bigboss.knowstar.entity.Role;
import com.bigboss.knowstar.entity.Tag;
import com.bigboss.knowstar.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface UserMapper {
    int createUser(User user);
    int updateUser(Integer Uid, Map<String, Object> fields);
    int deleteUser(Integer Uid);
    User getUser(Integer Uid);
    User loadUserByUsername(String UStuId);
    List<Role> getUserRolesByUid(Integer Uid);
    int isPhoneExist(String Uphone);
    int isStuIdExist(String UStuId);
    int createUserTag(Integer Uid, Integer Tid);
    List<Tag> getUserTags(Integer Uid);
    int resetPassword(String Uphone, String newPassword);
    // int getTotalVisits(Integer Uid); 获取某个用户的访问量
    // List<User> getAllFans(Integer Uid); 获取某个用户所有粉丝
    // List<User> getAllFollowers(Integer Uid); 获取某个用户所有关注的人
    // int getTotalViews(Integer Uid);  获取某个用户所有文档的浏览总量
    // int getTotalDownloads(Integer Uid); 获取某个用户所有文档的下载总量
    // int getTotalLikes(Integer Uid); 获取某个用户所有文档的点赞总量
}
