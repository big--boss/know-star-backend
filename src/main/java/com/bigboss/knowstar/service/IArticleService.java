package com.bigboss.knowstar.service;

import com.bigboss.knowstar.entity.Article;

import java.util.List;
import java.util.Map;

public interface IArticleService {
    void createArticle(Article article);
    void updateArticle(Integer Aid, Map<String, Object> fields);
    void deleteArticle(Integer Aid);
    Article getArticle(Integer Aid);
    List<Article> getAllArticle();
}
