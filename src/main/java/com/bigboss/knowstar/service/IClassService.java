package com.bigboss.knowstar.service;

import com.bigboss.knowstar.entity.Class;
import com.bigboss.knowstar.entity.Homework;
import com.bigboss.knowstar.entity.Notice;
import com.bigboss.knowstar.entity.User;
import com.bigboss.knowstar.entity.model.ClassItemData;

import java.util.List;
import java.util.Map;

public interface IClassService {
    void createClass(Class class_);
    void updateClass(Integer ClassId, Map<String, Object> fields);
    void deleteClass(Integer ClassId);
    Class getClass(Integer ClassId);
    List<User> getClassMembers(Integer ClassId);
    List<Homework> getClassHomework(Integer ClassId);
    List<Notice> getClassNotices(Integer ClassId);
    void addClassMember(Integer ClassId, String UStuId);
    boolean isMemberExist(Integer ClassId, String UStuId);
    List<ClassItemData> getAllClasses(Integer grade);
}
