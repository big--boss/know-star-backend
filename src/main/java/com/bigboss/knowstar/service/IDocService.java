package com.bigboss.knowstar.service;

import com.bigboss.knowstar.entity.Doc;
import com.bigboss.knowstar.entity.Tag;

import java.util.List;
import java.util.Map;

public interface IDocService {
    void createDoc(Integer Fid, Doc doc);
    void updateDoc(Integer Did, Map<String, Object> fields);
    void deleteDoc(Integer Did);
    Doc getDoc(Integer Did);
    void createDocTag(Integer Did, Tag tag);
    List<Tag> getDocTags(Integer Did);
}
