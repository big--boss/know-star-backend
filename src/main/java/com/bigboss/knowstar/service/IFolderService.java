package com.bigboss.knowstar.service;

import com.bigboss.knowstar.entity.Doc;
import com.bigboss.knowstar.entity.Folder;
import com.bigboss.knowstar.entity.Tag;

import java.util.List;
import java.util.Map;

public interface IFolderService {
    void createFolder(Folder folder);
    void updateFolder(Integer Fid, Map<String, Object> fields);
    void deleteFolder(Integer Fid);
    Folder getFolder(Integer Fid);
    List<Folder> getAllFolder();
    List<Doc> getAllDocInFolder(Integer Fid);
    void createFolderTag(Integer Fid, Tag tag);
    List<Tag> getFolderTags(Integer Fid);
}
