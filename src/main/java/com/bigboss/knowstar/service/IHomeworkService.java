package com.bigboss.knowstar.service;

import com.bigboss.knowstar.entity.Homework;

import java.util.Map;

public interface IHomeworkService {
    void createHomework(Integer ClassId, Homework homework);
    void updateHomework(Integer Hid, Map<String, Object> fields);
    void deleteHomework(Integer Hid);
    Homework getHomework(Integer Hid);
}
