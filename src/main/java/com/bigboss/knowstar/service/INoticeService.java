package com.bigboss.knowstar.service;

import com.bigboss.knowstar.entity.Notice;

import java.util.Map;

public interface INoticeService {
    void createNotice(Integer ClassId, Notice notice);
    void updateNotice(Integer Nid, Map<String, Object> fields);
    void deleteNotice(Integer Nid);
    Notice getNotice(Integer Nid);
    Notice getLatestNotice();
}
