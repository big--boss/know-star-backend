package com.bigboss.knowstar.service;

import java.util.Map;

public interface ISmsService {
    public boolean send(String phoneNum, String templateCode, Map<String, Object> code);
}
