package com.bigboss.knowstar.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface IUploadService {
    String uploadFile(MultipartFile file, HttpServletRequest req, String type);
    List<String> multiUploadFiles(MultipartFile[] files, HttpServletRequest req, String type);
}
