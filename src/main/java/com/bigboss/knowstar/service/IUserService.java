package com.bigboss.knowstar.service;

import com.bigboss.knowstar.entity.Tag;
import com.bigboss.knowstar.entity.User;

import java.util.List;
import java.util.Map;

public interface IUserService {
    void createUser(User user);
    void updateUser(Integer Uid, Map<String, Object> fields);
    void deleteUser(Integer Uid);
    User getUser(Integer Uid);
    boolean isPhoneExist(String Uphone);
    boolean isStuIdExist(String UStuId);
    void createUserTag(Integer Uid, Tag tag);
    List<Tag> getUserTags(Integer Uid);
    void resetPassword(String Uphone, String newPassword);
}
