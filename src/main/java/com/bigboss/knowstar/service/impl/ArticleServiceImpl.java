package com.bigboss.knowstar.service.impl;

import com.bigboss.knowstar.entity.Article;
import com.bigboss.knowstar.exception.ArticleException;
import com.bigboss.knowstar.mapper.ArticleMapper;
import com.bigboss.knowstar.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl implements IArticleService {

    @Autowired
    private ArticleMapper articleMapper;

    @Override
    public void createArticle(Article article) {
        if (article.getAtitle() == null) {
            throw new ArticleException("请填写文章标题");
        }
        article.setAimage("");
        article.setAcreateTime(new Date());
        article.setAvisible(true);
        article.setAsource("机器之心");
        int res = articleMapper.createArticle(article);
        if (res == 0) {
            throw new ArticleException("文章创建失败");
        }
    }

    @Override
    public void updateArticle(Integer Aid, Map<String, Object> fields) {
        int res = articleMapper.updateArticle(Aid, fields);
        if (res == 0) {
            throw new ArticleException("更新文章信息失败");
        }
    }

    @Override
    public void deleteArticle(Integer Aid) {
        int res = articleMapper.deleteArticle(Aid);
        if (res == 0) {
            throw new ArticleException("删除文章失败");
        }
    }

    @Override
    public Article getArticle(Integer Aid) {
        Article article = articleMapper.getArticle(Aid);
        if (article == null) {
            throw new ArticleException("当前文章不存在");
        }
        return article;
    }

    @Override
    public List<Article> getAllArticle() {
        return articleMapper.getAllArticle();
    }
}
