package com.bigboss.knowstar.service.impl;

import com.bigboss.knowstar.entity.*;
import com.bigboss.knowstar.entity.Class;
import com.bigboss.knowstar.entity.model.ClassItemData;
import com.bigboss.knowstar.entity.model.LoginUser;
import com.bigboss.knowstar.exception.ClassException;
import com.bigboss.knowstar.exception.DocException;
import com.bigboss.knowstar.exception.UserException;
import com.bigboss.knowstar.mapper.ClassMapper;
import com.bigboss.knowstar.mapper.UserMapper;
import com.bigboss.knowstar.service.IClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ClassServiceImpl implements IClassService {

    @Autowired
    private ClassMapper classMapper;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private NoticeServiceImpl noticeService;

    @Override
    public void createClass(Class class_) {
        if (class_.getClassName() == null) {
            throw new ClassException("请填写班级名称");
        }
        class_.setClassCreateTime(new Date());
        class_.setClassVisible(true);
        class_.setClassStuNum(0);
        if (LoginUser.getLoginUser() instanceof String) {
            throw new ClassException("请登录后创建班级");
        } else {
            User user = (User) LoginUser.getLoginUser();
            class_.setClassAdminId(user.getUid());
        }
        int res = classMapper.createClass(class_);
        if (res == 0) {
            throw new ClassException("班级创建失败");
        }
    }

    @Override
    public void updateClass(Integer ClassId, Map<String, Object> fields) {
        int res = classMapper.updateClass(ClassId, fields);
        if (res == 0) {
            throw new ClassException("更新班级信息失败");
        }
    }

    @Override
    public void deleteClass(Integer ClassId) {
        int res = classMapper.deleteClass(ClassId);
        if (res == 0) {
            throw new ClassException("删除班级失败");
        }
    }

    @Override
    public Class getClass(Integer ClassId) {
        Class class_ = classMapper.getClass(ClassId);
        if (class_ == null) {
            throw new ClassException("当前班级不存在");
        }
        return class_;
    }

    @Override
    public List<User> getClassMembers(Integer ClassId) {
        return classMapper.getClassMembers(ClassId);
    }

    @Override
    public List<Homework> getClassHomework(Integer ClassId) {
        return classMapper.getClassHomework(ClassId);
    }

    @Override
    public List<Notice> getClassNotices(Integer ClassId) {
        return classMapper.getClassNotices(ClassId);
    }

    @Override
    public void addClassMember(Integer ClassId, String UStuId) {
        if (!userService.isStuIdExist(UStuId)) {
            throw new UserException("用户 " + UStuId + " 不存在");
        } else if (isMemberExist(ClassId, UStuId)) {
            throw new ClassException(UStuId + " 已加入当前班级");
        } else {
            int res = classMapper.addClassMember(ClassId, UStuId);
            if (res == 0) {
                throw new ClassException(UStuId + " 加入班级失败");
            }
        }
    }

    @Override
    public boolean isMemberExist(Integer ClassId, String UStuId) {
        return classMapper.isMemberExist(ClassId, UStuId) == 1;
    }

    @Override
    public List<ClassItemData> getAllClasses(Integer grade) {
        List<ClassItemData> classList = new ArrayList<>();
        List<Class> allClasses;
        if (grade == 0) {
            // grade为空，表示查询所有的班级
            allClasses = classMapper.getAllClasses();
        } else {
            // grade不为空，表示查询对象年级的班级
            allClasses = classMapper.getAllClassesByGrade(grade);
        }
        for (Class _class: allClasses){
            ClassItemData classItemData = new ClassItemData();
            classItemData.set_class(_class);
            classItemData.setClassAdmin(userService.getUser(_class.getClassAdminId()));
            classItemData.setClassLatestNotice(noticeService.getLatestNotice());
            classList.add(classItemData);
        }
        return classList;
    }
}
