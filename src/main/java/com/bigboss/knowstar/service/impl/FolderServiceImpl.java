package com.bigboss.knowstar.service.impl;

import com.bigboss.knowstar.entity.Doc;
import com.bigboss.knowstar.entity.Folder;
import com.bigboss.knowstar.entity.Tag;
import com.bigboss.knowstar.entity.User;
import com.bigboss.knowstar.entity.model.LoginUser;
import com.bigboss.knowstar.exception.FolderException;
import com.bigboss.knowstar.exception.TagException;
import com.bigboss.knowstar.mapper.FolderMapper;
import com.bigboss.knowstar.mapper.TagMapper;
import com.bigboss.knowstar.service.IFolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import sun.plugin.liveconnect.SecurityContextHelper;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class FolderServiceImpl implements IFolderService {

    @Autowired
    private FolderMapper folderMapper;

    @Autowired
    private TagMapper tagMapper;

    @Override
    public void createFolder(Folder folder) {
        if (folder.getFname() == null) {
            throw new FolderException("请填写专栏名称");
        }
        folder.setFcreateTime(new Date());
        folder.setFvisible(true);
        folder.setFimage("");
        try {
            Object principal = LoginUser.getLoginUser();
            // 获取当前登录用户对象
            if (principal != null) {
                User user = (User) principal;
                folder.setFUid(user.getUid());
            }
        } catch (ClassCastException e) {
            throw new FolderException("请登录后再创建专栏");
        }
        int res = folderMapper.createFolder(folder);
        if (res == 0) {
            throw new FolderException("专栏创建失败");
        }
    }

    @Override
    public void updateFolder(Integer Fid, Map<String, Object> fields) {
        int res = folderMapper.updateFolder(Fid, fields);
        if (res == 0) {
            throw new FolderException("更新专栏信息失败");
        }
    }

    @Override
    public void deleteFolder(Integer Fid) {
        int res = folderMapper.deleteFolder(Fid);
        if (res == 0) {
            throw new FolderException("删除专栏失败");
        }
    }

    @Override
    public Folder getFolder(Integer Fid) {
        Folder folder = folderMapper.getFolder(Fid);
        if (folder == null) {
            throw new FolderException("当前专栏不存在");
        }
        return folder;
    }

    @Override
    public List<Folder> getAllFolder() {
        return folderMapper.getAllFolder();
    }

    @Override
    public List<Doc> getAllDocInFolder(Integer Fid) {
        return folderMapper.getAllDocInFolder(Fid);
    }

    @Override
    public void createFolderTag(Integer Fid, Tag tag) {
        int Tid = tagMapper.isTnameExist(tag.getTname());
        if (Tid == 0) {
            // 标签名不存在，则创建新的标签
            tag.setTcreateTime(new Date());
            tag.setTvisible(true);
            int res = tagMapper.createTag(tag);
            if (res == 0) {
                throw new TagException("标签创建失败");
            }
            Tid = tagMapper.getTagByTname(tag.getTname()).getTid();
        }
        // 标签名存在
        folderMapper.createFolderTag(Fid, Tid);
    }

    @Override
    public List<Tag> getFolderTags(Integer Fid) {
        return folderMapper.getFolderTags(Fid);
    }
}
