package com.bigboss.knowstar.service.impl;

import com.bigboss.knowstar.entity.Homework;
import com.bigboss.knowstar.exception.HomeworkException;
import com.bigboss.knowstar.mapper.HomeworkMapper;
import com.bigboss.knowstar.service.IHomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class HomeworkServiceImpl implements IHomeworkService {

    @Autowired
    private HomeworkMapper homeworkMapper;

    @Override
    public void createHomework(Integer ClassId, Homework homework) {
        if (homework.getHtitle() == null) {
            throw new HomeworkException("请填写班级名称");
        }
        homework.setHcreateTime(new Date());
        homework.setHvisible(true);
        homework.setHsubmits(0);
        homework.setHClassId(ClassId);
        int res = homeworkMapper.createHomework(homework);
        if (res == 0) {
            throw new HomeworkException("作业创建失败");
        }
    }

    @Override
    public void updateHomework(Integer Hid, Map<String, Object> fields) {
        int res = homeworkMapper.updateHomework(Hid, fields);
        if (res == 0) {
            throw new HomeworkException("更新作业信息失败");
        }
    }

    @Override
    public void deleteHomework(Integer Hid) {
        int res = homeworkMapper.deleteHomework(Hid);
        if (res == 0) {
            throw new HomeworkException("删除作业失败");
        }
    }

    @Override
    public Homework getHomework(Integer Hid) {
        Homework homework = homeworkMapper.getHomework(Hid);
        if (homework == null) {
            throw new HomeworkException("当前作业不存在");
        }
        return homework;
    }
}
