package com.bigboss.knowstar.service.impl;

import com.bigboss.knowstar.entity.Notice;
import com.bigboss.knowstar.exception.NoticeException;
import com.bigboss.knowstar.mapper.NoticeMapper;
import com.bigboss.knowstar.service.INoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class NoticeServiceImpl implements INoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public void createNotice(Integer ClassId, Notice notice) {
        if (notice.getNtitle() == null) {
            throw new NoticeException("请填写公告标题");
        }
        notice.setNcreateTime(new Date());
        notice.setNvisible(true);
        notice.setNClassId(ClassId);
        int res = noticeMapper.createNotice(notice);
        if (res == 0) {
            throw new NoticeException("公告创建失败");
        }
    }

    @Override
    public void updateNotice(Integer Nid, Map<String, Object> fields) {
        int res = noticeMapper.updateNotice(Nid, fields);
        if (res == 0) {
            throw new NoticeException("更新公告信息失败");
        }
    }

    @Override
    public void deleteNotice(Integer Nid) {
        int res = noticeMapper.deleteNotice(Nid);
        if (res == 0) {
            throw new NoticeException("删除公告失败");
        }
    }

    @Override
    public Notice getNotice(Integer Nid) {
        Notice notice = noticeMapper.getNotice(Nid);
        if (notice == null) {
            throw new NoticeException("当前公告不存在");
        }
        return notice;
    }

    @Override
    public Notice getLatestNotice() {
        return noticeMapper.getLatestNotice();
    }
}
