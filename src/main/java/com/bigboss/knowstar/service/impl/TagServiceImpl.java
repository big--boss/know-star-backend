package com.bigboss.knowstar.service.impl;

import com.bigboss.knowstar.entity.Tag;
import com.bigboss.knowstar.exception.TagException;
import com.bigboss.knowstar.mapper.TagMapper;
import com.bigboss.knowstar.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class TagServiceImpl implements ITagService {

    @Autowired
    private TagMapper tagMapper;

    @Override
    public void createTag(Tag tag) {
        if (tag.getTname() == null) {
            throw new TagException("请填写标签名称");
        }
        tag.setTcreateTime(new Date());
        tag.setTvisible(true);
        int res = tagMapper.createTag(tag);
        if (res == 0) {
            throw new TagException("标签创建失败");
        }
    }

    @Override
    public void updateTag(Integer Tid, Map<String, Object> fields) {
        int res = tagMapper.updateTag(Tid, fields);
        if (res == 0) {
            throw new TagException("更新标签信息失败");
        }
    }

    @Override
    public void deleteTag(Integer Tid) {
        int res = tagMapper.deleteTag(Tid);
        if (res == 0) {
            throw new TagException("删除标签失败");
        }
    }

    @Override
    public Tag getTag(Integer Tid) {
        Tag tag = tagMapper.getTag(Tid);
        if (tag == null) {
            throw new TagException("当前标签不存在");
        }
        return tag;
    }

    @Override
    public Tag getTagByTname(String Tname) {
        Tag tag = tagMapper.getTagByTname(Tname);
        if (tag == null) {
            throw new TagException("当前标签不存在");
        }
        return tag;
    }

    @Override
    public int isTnameExist(String Tname) {
        // 如果存在，返回该标签的Tid
        // 如果不存在，返回0
        return tagMapper.isTnameExist(Tname);
    }

    @Override
    public List<Tag> getTagsByUid(Integer Uid) {
        return tagMapper.getTagsByUid(Uid);
    }

    @Override
    public List<Tag> getTagsByFid(Integer Fid) {
        return tagMapper.getTagsByFid(Fid);
    }

    @Override
    public List<Tag> getTagsByAid(Integer Aid) {
        return tagMapper.getTagsByAid(Aid);
    }
}
