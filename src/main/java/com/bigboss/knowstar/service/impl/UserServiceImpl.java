package com.bigboss.knowstar.service.impl;

import com.bigboss.knowstar.entity.Tag;
import com.bigboss.knowstar.entity.User;
import com.bigboss.knowstar.exception.TagException;
import com.bigboss.knowstar.exception.UserException;
import com.bigboss.knowstar.mapper.TagMapper;
import com.bigboss.knowstar.mapper.UserMapper;
import com.bigboss.knowstar.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements IUserService, UserDetailsService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void createUser(User user) {
        if (user.getUphone() == null || user.getUname() == null) {
            throw new UserException("请正确填写表单信息");
        }
        if (user.getUphone() != null && isPhoneExist(user.getUphone())) {
            throw new UserException("当前手机号码已存在");
        }
        if (user.getUStuId() != null && isStuIdExist(user.getUStuId())) {
            throw new UserException("当前学号已存在");
        }
        if (user.getUphone() != null && user.getPassword() != null) {
            user.setUcreateTime(new Date());
            user.setUvisible(true);
            user.setUvisits(0);
            user.setUpassword(passwordEncoder.encode(user.getUpassword()));
            int res = userMapper.createUser(user);
            if (res == 0) {
                throw new UserException("用户注册失败");
            }
        } else {
            throw new UserException("请填写登录密码");
        }
    }

    @Override
    public void updateUser(Integer Uid, Map<String, Object> fields) {
        if (fields.containsKey("Upassword") && fields.get("Upassword") != null) {
            fields.put("Upassword", passwordEncoder.encode((String) fields.get("Upassword")));
        }
        int res = userMapper.updateUser(Uid, fields);
        if (res == 0) {
            throw new UserException("更新用户信息失败");
        }
    }

    @Override
    public void deleteUser(Integer Uid) {
        int res = userMapper.deleteUser(Uid);
        if (res == 0) {
            throw new UserException("删除用户失败");
        }
    }

    @Override
    public User getUser(Integer Uid) {
        User user = userMapper.getUser(Uid);
        if (user == null) {
            throw new UserException("当前用户不存在");
        }
        return user;
    }

    @Override
    public boolean isPhoneExist(String Uphone) {
        return userMapper.isPhoneExist(Uphone) == 1;
    }

    @Override
    public boolean isStuIdExist(String UStuId) {
        return userMapper.isStuIdExist(UStuId) == 1;
    }

    @Override
    public void createUserTag(Integer Uid, Tag tag) {
        int Tid = tagMapper.isTnameExist(tag.getTname());
        if (Tid == 0) {
            // 标签名不存在，则创建新的标签
            tag.setTcreateTime(new Date());
            tag.setTvisible(true);
            int res = tagMapper.createTag(tag);
            if (res == 0) {
                throw new TagException("标签创建失败");
            }
            Tid = tagMapper.getTagByTname(tag.getTname()).getTid();
        }
        // 标签名存在
        userMapper.createUserTag(Uid, Tid);
    }

    @Override
    public List<Tag> getUserTags(Integer Uid) {
        return userMapper.getUserTags(Uid);
    }

    @Override
    public UserDetails loadUserByUsername(String UStuId) throws UsernameNotFoundException {
        User user = userMapper.loadUserByUsername(UStuId);
        if (user == null) {
            throw new UsernameNotFoundException("当前用户不存在");
        }
        user.setRoles(userMapper.getUserRolesByUid(user.getUid()));
        return user;
    }

    @Override
    public void resetPassword(String Uphone, String newPassword) {
        int res = userMapper.resetPassword(Uphone, passwordEncoder.encode(newPassword));
        if (res == 0) {
            throw new UserException("重置密码失败");
        }
    }
}
