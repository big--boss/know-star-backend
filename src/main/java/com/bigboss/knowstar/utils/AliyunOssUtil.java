package com.bigboss.knowstar.utils;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.ObjectMetadata;
import com.bigboss.knowstar.exception.FileUploadException;

import java.io.InputStream;

public class AliyunOssUtil {
    public static final String ENDPOINT = "oss-cn-hangzhou.aliyuncs.com";
    public static final String ACCESSKEYID = "LTAI4G9wFT7biAnMnhQzhJzt";
    public static final String ACCESSKEYSECRET = "zFUYAS4rc7Habvvm6UqyboJzHjc9rV";
    public static final String BUCKETNAME = "oss-knowstar";

    public static OSS ossClient = new OSSClientBuilder().build(ENDPOINT, ACCESSKEYID, ACCESSKEYSECRET);

    public static String ossUploadFile(String fileName, InputStream inputStream, ObjectMetadata metadata) {
        try {
            ossClient.putObject(BUCKETNAME, fileName, inputStream, metadata);
            return "https://" + BUCKETNAME + "." + ENDPOINT + "/" + fileName;
        } catch (OSSException e) {
            throw new FileUploadException("OSS远端拒绝了上传请求");
        } catch (ClientException e) {
            throw new FileUploadException("连接OSS时出错");
        }
    }

}
