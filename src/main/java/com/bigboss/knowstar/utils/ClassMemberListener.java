package com.bigboss.knowstar.utils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelAnalysisException;
import com.bigboss.knowstar.entity.model.ClassMemberData;
import com.bigboss.knowstar.exception.ClassException;
import com.bigboss.knowstar.exception.UserException;
import com.bigboss.knowstar.service.impl.ClassServiceImpl;

public class ClassMemberListener extends AnalysisEventListener<ClassMemberData> {

    private ClassServiceImpl classService;
    private Integer ClassId;

    public ClassMemberListener(ClassServiceImpl classService, Integer ClassId) {
        this.classService = classService;
        this.ClassId = ClassId;
    }

    @Override
    public void invoke(ClassMemberData data, AnalysisContext context) {
        System.out.println(data.getUStuId());
        try {
            classService.addClassMember(ClassId, data.getUStuId());
        } catch (UserException | ClassException e) {
            throw new ExcelAnalysisException(e.getMessage());
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//        System.out.println("数据导入完毕");
    }
}
