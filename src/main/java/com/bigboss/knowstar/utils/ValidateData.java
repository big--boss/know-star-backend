package com.bigboss.knowstar.utils;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class ValidateData {
    /***
     * 传入result，如果有错误则返回错误信息数组，如果没有错误则返回null
     * @param result
     * @return
     */
    static public ArrayList<String> validateData(BindingResult result) {
        if (result.hasErrors()) {
            ArrayList<String> errors = new ArrayList<>();
            List<ObjectError> allErrors = result.getAllErrors();
            for (ObjectError error : allErrors) {
                errors.add(error.getDefaultMessage());
            }
            return errors;
        } else {
            return null;
        }
    }
}
