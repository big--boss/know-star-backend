package com.bigboss.knowstar.utils;

import com.bigboss.knowstar.entity.model.APIResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class VerificationCodeFilter extends GenericFilter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        System.out.println("path: " + req.getServletPath());
        if ("POST".equals(req.getMethod()) && "/api/login".equals(req.getServletPath())) {
            String code = req.getParameter("code");
            String verifyCode = (String) req.getSession().getAttribute("verify_code");
            System.out.println("code= " + code);
            System.out.println("verify_code= " + verifyCode);
            if (code == null || verifyCode == null ||  "".equals(code) || !verifyCode.toLowerCase().equals(code.toLowerCase())) {
                resp.setContentType("application/json;charset=utf-8");
                PrintWriter out = resp.getWriter();
                out.write(new ObjectMapper().writeValueAsString(APIResult.error("验证码错误")));
                out.flush();
                out.close();
            } else {
                filterChain.doFilter(req, resp);
            }
        } else {
            filterChain.doFilter(req, resp);
        }
    }
}
